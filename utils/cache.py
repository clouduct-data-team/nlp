# -*- coding: utf-8 -*-

"""
class MemcachedCache(object):
    from werkzeug.contrib.cache import MemcachedCache
    cache = MemcachedCache(['127.0.0.1:11211'])

class Bmemcached(object):
    import bmemcached
    cache = bmemcached.Client(('localhost:11211',),'admin','fJ4qjsjlpf0k')

class Pylibmc(object):
    import pylibmc
    cache = pylibmc.Client(['localhost:11211'], binary=True,
        username='admin', password='fJ4qjsjlpf0k',
        behaviors={
            "tcp_nodelay": True,
            "no_block": True,
            # 设置get/set的超时时间
            "_poll_timeout": 2000,
    })
"""

def get_cache_instance(host, port):
    import redis
    cache = redis.StrictRedis(host=host, port=port, db=0)
    return cache

def get_identifier(keyword, city_id, latlng):
    identifier = keyword + city_id + latlng
    identifier = identifier.encode('utf-8', 'ignore')
    return identifier
