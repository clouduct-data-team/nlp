# -*- coding: utf-8 -*-
import json
import time 

class Txt_Validator(object):

    def __init__(self):
        self.invalid_json_counter = 0
        self.json_conter_error = 0
        self.sn2gid_counts, self.sn2gid = 0, {}
        self.sn2mn_error, self.sn2mn = 0, {}
        self.gid2sn_error, self.gid2sn = 0 , {} 
        self.gid2latlng_error, self.low_latlng_accuracy_error, self.gid2latlng = 0, 0, {}

    def get_report(self, txt_file=None, detail=None):
        if txt_file is not None:
            with open(txt_file) as f:
                 for line in f:
                    self.validate_line(line)

        report = {}
        report['sn2gid_counts'] = self.sn2gid_counts
        report['sn2mn_error'] = self.sn2mn_error 
        report['gid2sn_error'] = self.gid2sn_error 
        report['gid2latlng_error'] = self.gid2latlng_error
        report['low_latlng_accuracy_error'] = self.low_latlng_accuracy_error
        return report

    def validate_line(self, line):
        obj = self.get_valid_json(line)
        self.validate_sn2gid_mapping(obj) 
        self.validate_sn2mn_mapping(obj)
        self.validate_gid2latlng_mapping(obj)
        self.vlidate_gid2sn_mapping(obj)


    def get_valid_json(self, line):
        obj = {}
        try:
            obj = json.loads(line)
        except Exception as e:
            self.invalid_json_counter += 1
            line = line.strip().replace(":\"0\"\"","").\
                                replace(":\"-1\"\"","")
            try:
                obj = json.loads(line)
            except Exception as e:
                self.json_conter_error += 0
                # TO-DO: Log line
        return obj

    def validate_sn2gid_mapping(self, obj):
        # check if one showname map to multiple gid
        if 'showname' in obj:
            gid = obj['groupid']
            showname = obj['showname']

            if showname not in self.sn2gid:
                self.sn2gid[showname] = gid

            if gid != self.sn2gid[showname]:
                self.sn2gid_counts += 1

    def validate_sn2mn_mapping(self, obj):
        # whether check one showname map to multiple showmainname
        if 'showname' in obj and 'showmainname' in obj:
            showname = obj['showname']
            showmainname = obj['showmainname']
            if showname not in self.sn2mn:
                self.sn2mn[showname] = showmainname
            if showmainname != self.sn2mn[showname]:
                self.sn2mn_error += 1

    def vlidate_gid2sn_mapping(self, obj):
        # check if one gid map to multiple name
        if 'showname' in obj:
            gid = obj['groupid']
            showname = obj['showname']
            if showname not in self.gid2sn:
                self.gid2sn[gid] = showname 
            if showname != self.gid2sn[gid]:
                self.gid2sn_error += 1

    def validate_gid2latlng_mapping(self, obj):

        if 'lat' in obj:
            gid = obj['groupid']
            if  len(obj['lat']) <  7:
                # 12.4567 accuracy must grate than 7 characters.
                self.low_latlng_accuracy_error += 1
            try:
                lat = round(float(obj['lat']), 4)
            except Exception as e:
                print  obj['lat']
                return None

            if gid not in self.gid2latlng: 
                self.gid2latlng[gid] = lat

            if lat != self.gid2latlng[gid]:
                self.gid2latlng_error += 1





def timeit(f):
    # Decorator for computing execution time
    # Refernce: http://tinyurl.com/zkrje4f

    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        print 'func:%r - took: %2.4f sec' % (f.__name__, te-ts)
        #print 'func:%r args:[%r, %r] took: %2.4f sec' % \
        #  (f.__name__, args, kw, te-ts)
        return result
    return timed


