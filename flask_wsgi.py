#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import *
#Flask, url_for, render_template, request, session, redirect, abort, Response
import subprocess
import json
import random
import traceback
import os
import time
import json
from flask import render_template
#import app, db, login_manager, bcrypt
from flask import Flask, session
from flask.ext.session import Session
from utils.nocache import nocache
from flask.ext.sqlalchemy import SQLAlchemy
#from bcrypt import *
#import bcrypt
from flask.ext.cors import CORS
from flask import jsonify
from pprintpp import pprint as pp
from bson import json_util
from sqlalchemy import *
from api_views import api
from ui_views import ui

from instance import config as settings 
    
application= Flask(__name__)
#application.secret_key = 'super secret key'
application.config['SESSION_TYPE'] = 'filesystem'
application.config['SECRET_KEY'] = 'super secret key'
# https://mofanim.wordpress.com/2013/01/02/sqlalchemy-mysql-has-gone-away/
application.debug = True
application.config['SETTINGS'] = settings
# TO-DO:
#    Try out using app.config.from_pyfile(config_filename) and
#    access settings in views by current_app function.
# Reference: http://docs.jinkan.org/docs/flask/patterns/appfactories.html

cors = CORS(application)
sess = Session()
sess.init_app(application)
application.register_blueprint(api)
application.register_blueprint(ui)

if __name__ == "__main__":
    application.run()
