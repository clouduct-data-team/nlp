import os
import logging
import logging.config
import yaml
import logstash

def get_loggging_config(path):
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.load(f.read())
            return config
            #logging.config.dictConfig(config)


def append_logstash_handler(logger, host, port):
    logger.setLevel(logging.INFO)
    logger.addHandler(logstash.TCPLogstashHandler(host, port, version=1))


