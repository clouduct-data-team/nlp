#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import *
#Flask, url_for, render_template, request, session, redirect, abort, Response
import subprocess
import json
import random
import traceback
import os
import time
import json
from flask import render_template
#import app, db, login_manager, bcrypt
from flask import Flask, session
from flask.ext.session import Session
from utils.nocache import nocache
from flask.ext.sqlalchemy import SQLAlchemy
#from bcrypt import *
#import bcrypt
from flask.ext.cors import CORS
from flask import jsonify
from pprintpp import pprint as pp
from bson import json_util
from sqlalchemy import *
from api_views import api
from ui_views import ui


def create_app(settings):
    app = Flask(__name__)
    #app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.config['SECRET_KEY'] = 'super secret key'
    # https://mofanim.wordpress.com/2013/01/02/sqlalchemy-mysql-has-gone-away/
    app.debug = True
    app.config['SETTINGS'] = settings
    # TO-DO:
    #    Try out using app.config.from_pyfile(config_filename) and
    #    access settings in views by current_app function.
    # Reference: http://docs.jinkan.org/docs/flask/patterns/appfactories.html

    cors = CORS(app)
    sess = Session()
    sess.init_app(app)
    app.register_blueprint(api)
    app.register_blueprint(ui)
    return app


if __name__ == "__main__":
    from instance import config as settings 
    app = create_app(settings)
    app.run(host='0.0.0.0', port=settings.SERVER_PORT, 
                            threaded=True, 
                            use_reloader=False)
