#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import *
#Flask, url_for, render_template, request, session, redirect, abort, Response
import subprocess
import json
import random
import traceback
import os
import time
import json
from flask import render_template
#import app, db, login_manager, bcrypt
from flask import Flask, session
from flask.ext.session import Session
from utils.nocache import nocache
#from bcrypt import *
#import bcrypt
from instance import config
from flask.ext.cors import CORS
from flask import jsonify
from pprintpp import pprint as pp
#from bson import json_util
from sqlalchemy import *
from flask import Blueprint
import requests
import re
import lib
from lib import generate_json, generate_response_from_json
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial
# http://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
import logging
import copy 
from utils.cache import get_cache_instance, get_identifier



p_tree = {}
pinyin_tree = {}
id_city_mapping = {}
cityname_id_mapping = {}
freq_tree , latlng_tree = {}, {}
settings =  {}
logger = {}
cache = False


class RegisteringExampleBlueprint(Blueprint):
    """
    Accrding How to access app.config in a blueprint?
    http://stackoverflow.com/questions/18214612
    """
    def register(self, app, options, first_registration=False):

        global settings
        settings = app.config.get('SETTINGS')
        global p_tree 
        global freq_tree
        global latlng_tree
        global id_city_mapping , cityname_id_mapping
        global logger
        
        def _get_logger():
            logger = logging.getLogger(__name__)
            import logger_setter
            config = logger_setter.get_loggging_config('logging.yaml')
            logging.config.dictConfig(config)
            if settings.ENABLE_LOGSTASH:
                logger_setter.append_logstash_handler(logger, settings.LOGSTASH_HOST, settings.LOGSTASH_PORT)
            return logger
        logger = _get_logger()

        if settings.ENABLE_CACHE == True:
            global cache 
            cache = get_cache_instance(settings.REDIS_HOST, settings.REDIS_PORT)


        id_city_mapping = lib.get_city_id_mapping(settings.MAPPING_PATH)
        cityname_id_mapping = lib.get_cityname_id_mapping(settings.MAPPING_PATH)
        if settings.LOAD_CLUSTER == False:
            city_list = ['defaul']
        else:
            city_list = lib.get_cluster_citys(settings.CLUSTER_PATH, settings.CITY_LIST)

        for city in city_list:
            if city == 'default':
                data_path = settings.DATA_PATH
            else:
                data_path = "%s/%s/%s.txt" %(settings.CLUSTER_PATH, city, city)

            city = city.lower()
            p_tree[city] = {}
            r1 = lib.get_processed_data(txt_file=data_path,
                   load_limit=settings.DATA_LOAD_LIMIT)
            result = lib.get_merged_structure(mn2sns=r1['mn2sns'],
                   sn2latlng=r1['sn2latlng'], sn2_not_mn=r1['sn2_not_mn'])
            tree = lib.build_ptree_by_merged_structure(result,
                   mn_load_freq_threshold=settings.MN_LOAD_FREQ_THRESHOLD)

            if settings.MERGED_TREE == True:
                p_tree[city]['merged'] = tree
            else:
                p_tree[city]['latlng'] = p_tree[city]['freq'] = tree

            if settings.PINYIN == True:
                tree = lib.build_ptree_by_merged_structure(result, 
                    mn_load_freq_threshold=settings.MN_LOAD_FREQ_THRESHOLD,
                    pinyin = True)
                pinyin_tree[city] = {}
                pinyin_tree[city]['merged'] = tree
                pinyin_tree[city]['latlng'] = pinyin_tree[city]['freq'] = tree


        super(RegisteringExampleBlueprint, self).register(app, options, first_registration)
        """
        @api.record
        def record_params(setup_state):
            app = setup_state.app
            api.config = dict([(key,value) for (key,value) in app.config.iteritems()])
        """

api = RegisteringExampleBlueprint('app', __name__, template_folder='templates')
api.config = {}

def get_duration(fn):
    def timed(*args, **kw):
        ts = time.time()
        result = fn(*args, **kw)
        te = time.time()
        return str(te-ts)[:4], result
    return timed

class ConvertExceptions(object):
    func = None
    def __init__(self, exceptions, replacement=None):
        self.exceptions = exceptions
        self.replacement = replacement
    def __call__(self, *args, **kwargs):
        if self.func is None:
            self.func = args[0]
            return self
        try:
            return self.func(*args, **kwargs)
        except self.exceptions:
            print "error ! ", self.func.__name__
            return self.replacement

def get_prf(testing, base):
    def get_precision(testing, base):
        if [] in [testing, base]:
            return 0
        hit = 0
        for _t in testing:
            if _t in base:
                hit += 1
        return hit/float(len(testing))

    def get_recall(testing, base):
        if [] in [testing, base]:
            return 0
        hit = 0
        for _b in base:
            if _b in testing:
                hit += 1
        return hit/float(len(base)) 

    def get_f_measure(precision, recall):
        if 0 in [precision, recall]:
            return 0
        return 2* float((precision*recall))/ float((precision+recall))

    p = get_precision(testing, base)
    r = get_recall(testing, base)
    f= get_f_measure(p, r)
    result = {"p": p, "r": r, "f": f}
    for k, v in result.items():
        result[k] = round(v, 2)
    return result 

@api.route("/")
def index():
    #return render_template('index.html')
    return redirect(url_for('.autocomplete'))

@get_duration
@ConvertExceptions(Exception, [])
def get_baidu_search_suggestion(city, q, latlng):
    """
    To-Do: Figure out what's the latlng for each city.
    """
    url = u"http://map.baidu.com/su?wd=%s&cid=1&type=0&newmap=1" % q
    url += u"&b=(12939750.56%20%2C%204829251.47%20%3B%2013037158.56%20%2C%204850371.47)&t=1452670348464&pc_ver=2"
    r = requests.get(url, timeout=5)
    r.encoding = r.apparent_encoding
    result = []
    for item in r.json()['s']:
        result.append(item.split('$')[3])
    return result

@get_duration
@ConvertExceptions(Exception, [])
def get_amap_search_suggestion(city, q, latlng):
    """
    To-Do: Figure out url for individual city. city=110000 ?
    """
    url = "http://ditu.amap.com/service/poiTips?&city=110000&words=%s" % q
    r = requests.get(url, timeout=5)
    r.encoding =  r.apparent_encoding
    result = []
    for item in r.json()['data']['tip_list']:
        result.append(item['tip']['name'])
    return result


@get_duration
@ConvertExceptions(Exception, [])
def get_meituan_search_suggestion(city, q, latlng):
    """
    To-Do: Figure out url for individual city. ex: bj.....
    """
    url = 'http://bj.meituan.com/search/smartboxv2/%s' % q
    s = requests.Session()
    s.headers.update({'X-Requested-With' : 'XMLHttpRequest'})
    r = s.post(url, timeout=5)
    r.encoding =  r.apparent_encoding
    _ = r.json()
    result = []
    for item in r.json():
        gid = item['gid']
        acm = item['acm']
        _ = acm.replace(gid, '').replace('AsearchB','')
        m = re.match(r'\.(.+)\.\d+', _)
        result.append(m.group(1))
    return result


@get_duration
@ConvertExceptions(Exception, [])
def get_dianhua_search_suggestion(city, q, latlng):
    '''
    '''
    global cityname_id_mapping
    city_id = cityname_id_mapping[city]
    url = "https://apis.dianhua.cn/search/suggestion.php?&city_id=%s&keyword=%s" %(city_id, q)
    r = requests.get(url, timeout=5)
    r.encoding =  r.apparent_encoding
    return r.json()['suggestions']

@get_duration
@ConvertExceptions(Exception, [])
def get_testing_search_suggestion(city, q, latlng):
    global cityname_id_mapping
    city_id = cityname_id_mapping[city]
    url = "http://0.0.0.0:%s/autocomplete?keyword=%s&latlng=%s&city_id=%s" % \
            (str(config.SERVER_PORT), q, latlng, city_id) 
    r = requests.get(url, timeout=10)
    r.encoding =  r.apparent_encoding
    result = []
    return r.json()['suggestions']

def get_search_result_by_domain(domain, city, q, latlng):
    r = {}
    search_mapping = {
        'testing': get_testing_search_suggestion,
        'baidu': get_baidu_search_suggestion,
        'amap': get_amap_search_suggestion,
        'meituan': get_meituan_search_suggestion,
        'dianhua': get_dianhua_search_suggestion,
    }
    r['domain'] = domain 
    r['duration'], r['data'] = search_mapping[domain](city, q, latlng)
    return r

@nocache
@api.route("/get_measurements", methods=["GET"])
def get_side_by_side_search_suggestion():
    city = ""
    q = request.args.get('q', u'')
    response = {}
    response[u'q'] = q
    city = request.args.get('city', u'').strip()
    latlng = request.args.get('latlng', "39.919858, 116.399122")
    pp(q)
    pp(latlng)
    r = {'baidu': {}, 'amap': {}, 'meituan': {}, 'dianhua': {}, 'testing': {} }
    #if q == u"":
    #    response[u'data'] = r
    #    return generate_json(response)

    logging.getLogger("requests").setLevel(logging.WARNING)
    # http://stackoverflow.com/questions/11029717/how-do-i-disable-log-messages-from-the-requests-library

    pool = ThreadPool(len(r))
    partial_get_search_result_by_domain = partial(get_search_result_by_domain, city=city, q=q, latlng=latlng)
    results = pool.map(partial_get_search_result_by_domain, r.keys())
    # Using Map to do one-line multithreading 
    # Ref: https://segmentfault.com/a/1190000000414339
    pool.close()
    pool.join()
    for result in results:
        r[result['domain']] = {} 
        for key in ['duration', 'data']:
            r[result['domain']][key] = result[key]
    for domain in r.keys():
        r[domain]['stat'] = get_prf(r['testing']['data'], r[domain]['data'])
    
    response[u'data'] = r
    json_str = generate_json(response)
    return generate_response_from_json(json_str)

@api.route("/autocomplete", methods=["GET"])
def autocomplete():
    # SPEC
    # https://www.secure.yulore.com/doc/index.php/%E6%90%9C%E7%B4%A2%E8%AF%8D%E8%87%AA%E5%8A%A8%E5%AE%8C%E6%88%90%E6%8E%A5%E5%8F%A3
    global logger

    def get_args(request):
        arg = {}
        arg['keyword'] = request.args.get('keyword', u'')
        arg['app'] = request.args.get('app', u'')
        arg['uid'] = request.args.get('uid', u'')
        arg['apikey'] = request.args.get('apikey', u'')
        city_id = request.args.get('city_id', -1)
        arg['latlng'] = request.args.get('latlng', "")
        if city_id == -1:
            city_id = "2"
        arg['city_id'] = city_id
        global id_city_mapping
        city = id_city_mapping[city_id]
        arg['city'] = city

        if settings.LOAD_CLUSTER == False:
           arg['city'] = 'default'

        if arg['latlng'] == u"":
            current_latlng = (0, 0)
        else:
            current_latlng = (
              float(latlng.split(",")[0]),
              float(latlng.split(",")[1])
            )
        arg['current_latlng'] = current_latlng
        arg['clientip'] = request.remote_addr
        return arg 

    arg = get_args(request)
    if arg['keyword'] == u"":
        return generate_response_from_json("")
    city = arg['city']
    
    global cache 
    if cache:
        identifier = get_identifier(arg['keyword'], arg['city'], arg['latlng'])
        cached_result = cache.get(identifier)
        if cached_result is not None:
            if settings.ENABLE_LOGSTASH:
                arg['hit'] = True
                logger.info("", extra=arg)
            return generate_response_from_json(cached_result)

    page = request.args.get('page', 1)

    global freq_tree
    global latlng_tree
    global p_tree

    pinyin = False
    start_time = time.time()

    if settings.PINYIN == True and arg['keyword'].encode('utf-8').isalpha():
        tree = pinyin_tree[arg['city']]['merged']
        pinyin = True
    elif settings.MERGED_TREE == True:
        tree = p_tree[arg['city']]['merged']

    arg['search_by'] = 'pinyin' if pinyin else 'keyword'

    search_result = lib.get_query_result2(q=arg['keyword'], center=arg['current_latlng'],  \
                        trie=tree, pinyin=pinyin, timeout=settings.TIMEOUT)
    """
    lib.get_query_result(q=q, center=current_latlng, \
    trie=tree, rank="latlng", merged=settings.MERGED_TREE, \
    page=page)
    """
    end_time = time.time()

    arg['case_type'] = search_result['case_type'] 
    arg['duration'] = (end_time - start_time) * 1000
    
    def wrap_result(search_result, arg): 
        result = {}
        result['keyword'] = arg['keyword']
        result['suggestions'] = []
        if search_result is not None:
            result['status'] = 0
            arg['total'] = search_result['total']
            arg['num'] = len(search_result['data'])
            result['suggestions'] = []
            insert_keyword = True
            for item in search_result['data']:
                if arg['keyword'] == item[0]:
                    insert_keyword = False
                    result['suggestions'].insert(0, item[0])
                else:
                    #result['suggestions'].append({'priority': item[1], 'name': item[0] })
                    result['suggestions'].append(item[0])
            if insert_keyword:
                result['suggestions'].insert(0, arg['keyword'])
        else:
            result['status'] = 404

        arg['status'] =result['status']
        arg['suggestions'] = result['suggestions']
        return result
 
    result = wrap_result(search_result, arg)   

    if settings.ENABLE_LOGSTASH:
        arg['hit'] = False
        logger.info("", extra=arg)

    _json_str = generate_json(result)
    if cache:
        cache.set(identifier, _json_str)
    return generate_response_from_json(_json_str)
