[![build status](https://ci.gitlab.com/projects/5001/status.png?ref=master)](https://ci.gitlab.com/projects/5001?ref=master)
[![Codacy Badge](https://www.codacy.com/project/badge/03beb8b8b5bf46ec9f063b6115d064c8)](https://www.codacy.com)
[![codecov.io](http://codecov.io/gitlab/Huang/talos/coverage.svg?branch=master)](https://codecov.io/gitlab/ebc/insight_crawler)

## Prerequisite
    - python 2.7.x above, virtualenv


## Install
    - System Package(centos)
        - package for linear algebra algo
            - yum install -y epel-release python-pip python-virtualenv
            - yum install -y python-devel 
            - yum install -y gcc-c++ 
            - yum install -y lapack lapack-devel blas blas-devel

    - Python Package
        - virtualenv venv 
        - source venv/bin/activate
        - pip install -r requirements.txt

    - Known Issues
        - use 'requests[security]' or just use requests==2.5.3 to install requests instead
            - Reference:
                - http://stackoverflow.com/questions/29099404/ssl-insecureplatform-error-when-using-requests-package    
    - optional
        - vim for python
           - sudo yum install -y clang cmake
           - https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
           - Download https://github.com/j1z0/vim-config/blob/master/vimrc to ~/.vimrc
	   - open vim and :PluginInstall to install plug-in
       - pydict
          - cd ~/.vim/bundle
          - git clone https://github.com/rkulla/pydiction.git
     	  - let g:pydiction_location = '/root/.vim/bundle/pydiction/complete-dict' 
              - you can also add it to ~/.vimrc
       
## Configuration
    - Configurate settings
        - cp api/instance/config.sample.py api/instance/config.py 
        - modify settings.py according to your env


## Testing
    - Using py.test to test application
        - source venv/bin/activate
        - py.test -v 


## Execution
    - python api/backend.py 


## Access 
    - hit http://< ip >:< port > /autocomplete?q=北京


## Deployment
    - ELK 
        - cd docker/elk
   	- docker-compose up

    - Single container 
        - cd docker/dockerfile.flask-uwsgi-nginx
	- docker build -t webapp .
        - docker run -v /root/projects/autocomplete/data:/home/docker/code/app/data  \
		     -v /root/projects/autocomplete/instance/config.py:/home/docker/code/app/instance/config.py  \
                     -p  8080:80 
                     -d webapp

