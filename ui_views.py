#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import *
#Flask, url_for, render_template, request, session, redirect, abort, Response
import subprocess
import json
import random
import traceback
import os
import time
import json
from flask import render_template
#import app, db, login_manager, bcrypt
from flask import Flask, session
from flask.ext.session import Session
from utils.nocache import nocache
from flask.ext.sqlalchemy import SQLAlchemy
#from bcrypt import *
#import bcrypt
from instance import config
from flask.ext.cors import CORS
from pprintpp import pprint as pp
from sqlalchemy import *
from flask import Blueprint
import requests
import re
import sys
import lib

settings = {}
p_tree = {}
freq_tree , latlng_tree = {}, {}
settings =  {}
cityname_id_mapping = {} 
cities = []

class RegisteringExampleBlueprint(Blueprint):
    """
    Accrding How to access app.config in a blueprint?
    http://stackoverflow.com/questions/18214612
    """
    def register(self, app, options, first_registration=False):
        global settings 
        settings = app.config.get('SETTINGS')
        global cityname_id_mapping
        cityname_id_mapping = lib.get_cityname_id_mapping(settings.MAPPING_PATH)
        id_city_mapping = lib.get_city_id_mapping(settings.MAPPING_PATH)
        global cities
        cities_enable, cities_disable = [], []
        import operator
        sorted_dict = sorted(cityname_id_mapping.items(), key=operator.itemgetter(1))
        for city in sorted_dict:
            #print city
            if str(city[1]) in id_city_mapping and ( 
                (len(settings.CITY_LIST) == 0) or (id_city_mapping[str(city[1])].capitalize() in  settings.CITY_LIST)
                ):
                cities_enable.append(city[0])
            else:
                cities_disable.append(city[0])

        cities = cities_enable + [u'以下尚未開放'] + cities_disable[1:]
        super(RegisteringExampleBlueprint, self).register(app, options, first_registration)
    """
    @api.record
    def record_params(setup_state):
        app = setup_state.app
        api.config = dict([(key,value) for (key,value) in app.config.iteritems()])
    """

ui = RegisteringExampleBlueprint('app2', __name__, template_folder='templates')


def get_duration(fn):
    def timed(*args, **kw):
        ts = time.time()
        result = fn(*args, **kw)
        te = time.time()
        return te-ts, result
    return timed

@nocache
@ui.route("/sbs", methods=["GET"])
def sbs():
    global cities
   
    domains = [ 'meituan', 'baidu', 'amap', 'dianhua', 'testing']
    row_stats = ['f', 'p', 'r', 'duration']
    row_search_result = ['search-0', 'search-1', 'search-2', 'search-3', 
                         'search-4', 'search-5', 'search-6', 'search-7',
                         'search-8', 'search-9']
    return render_template("index.html", cities=cities, domains = domains, row_stats = row_stats, row_search_result = row_search_result)
