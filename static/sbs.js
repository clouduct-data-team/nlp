$(document).ready(function() {
	$("#search").click(function(e) {
		get_suggestions(e, $("#q").val(), true);
	});
	$('#q, #latlng').on('keyup keypress blur change', function(e) {
		$("#small-icon, #qqq").html("<i class=\"fa fa-spinner fa-pulse\"></i>");
		qw = $("#q").val();
		hit_search = false;
		if (e.keyCode == 13) {
			hit_search = true;
		}
		delay(function(){
			get_suggestions(e, qw, hit_search);
		}, 230);

		//get_suggestions(e);
	});
	$(".dropdown-menu a").click(function() {
		$("#city").text($(this).text());
		$("#city").val($(this).text());
	});
	$("#q").autocomplete({
		source: availableTags
	});
});


var availableTags = [];
var last_qw = "";
var get_suggestions= function(e, qw, hit_serach) {
	e.preventDefault();

	if (hit_serach == false) {
	  	if ($("#q").val() != qw) {
			console.log("miss-match: "+qw);
			$("#small-icon, #qqq").html("<i>&nbsp;&nbsp;&nbsp;</i>");
			return false;
		}
		if (last_qw == qw) {
			console.log("same query : "+qw);
			$("#small-icon, #qqq").html("<i>&nbsp;&nbsp;&nbsp;</i>");
			return false;
		}
	}
	console.log("make a query: " + qw);

	latlng = $("#latlng").val();
	city = $('#city').text();
	$.ajax({type: "get",
		url: "/get_measurements",
		data: { q: qw , latlng: latlng, city: city},
		success:function(result) {
			response = jQuery.parseJSON(result);
			if (response['q'] == $("#q").val()) {
				last_qw = qw;
				availableTags = response['data']['testing']['data'];

				$("#q").autocomplete( "close" );
				$("#q").autocomplete({
					source: function(request, resolve) {
						resolve(availableTags);
					}
				});
				$("#q").trigger('keydown');
				// http://stackoverflow.com/questions/6431459/jquery-autocomplete-trigger-change-event

				update_cells(response['data']);
				console.log(response['data']['testing']['data']);
				$("#small-icon, #qqq").html("<i>&nbsp;&nbsp;&nbsp;</i>");
			}
		}
	});
	//$("#small-icon").html("<i class=\"fa fa-align-left\"></i>");
	//$("#small-icon, #qqq").html("<i>&nbsp;&nbsp;&nbsp;</i>");
}

var delay = (function() {
	var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();

var update_cells = function(data_obj) {
	$.each(data_obj, function(key, value) {  
		$('#' + key+'-duration').html(value['duration']);
		$.each(value['stat'], function(stat_key, stat_val) {
			$('#' + key+'-'+stat_key).html(stat_val);
		});
		/*
		   $.each(value['data'], function(i, j) {
		   $('#' + key+ '-'+i).html(j);
		   });
		   */ 
		for (i = 0; i < 10 ;i++) {
			//console.log(key+"-"+i+":"+value['data'].length+":"+value['data'][i]);
			if (i < value['data'].length) {
				$('#' + key+ '-'+(i+1)).html(value['data'][i]);
			}
			else {
				$('#' + key+ '-'+(i+1)).html("");
			}
		}
	}); 
}
