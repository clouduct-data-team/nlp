# -*- coding: utf-8 -*-
import sys
from backend import *
from flask import url_for
import api_views
from backend import create_app
import pytest
import flask
import urllib2
import simplejson as json
from pprintpp import pprint as pp
import lib
from instance import config_test as settings
# http://damyanon.net/flask-series-testing/
from collections import namedtuple
LatLng = namedtuple('Latlng', ['lat', 'lng'])

@pytest.fixture
def client():
    app = create_app(settings)
    client = app.test_client()
    return client 

def test_get_prt():
    testing = ['a', 'b', 'c' , 'd', 'e']
    base = ['a', 'c', 'e', 'f', 'g']
    e = {'f': 0.6, 'p': 0.6, 'r': 0.6}
    assert e == api_views.get_prf(testing, base)

def test_get_prt2():
    testing = ['a', 'b', 'c' , 'd', 'e']
    base = ['a', 'b', 'c', 'd', 'e']
    e = {'f': 1.0, 'p': 1.0, 'r': 1.0}
    assert e == api_views.get_prf(testing, base)

"""
def test_build_prefix_tree_by_txt():
    expected = [
        (u'Yes煮茶家', LatLng(lat=39.974041,lng=116.326781)),
        (u'乐客主题餐吧', LatLng(lat=39.989264, lng=116.336495)),
        (u'凯菱国际酒店公寓', LatLng(lat=39.97139418, lng=116.3054855)),
        (u'北京祺才人本教育咨询有限责任公司', LatLng(lat=39.950028, lng=116.314855)),
        (u'圣米石塘', LatLng(lat=39.834689, lng=115.721503)),
        (u'小码头重庆火锅', LatLng(lat=39.959541, lng=116.335263)),
        (u'温馨花店', LatLng(lat=39.824474, lng=116.33565)),
        (u'湖南七星电视购物有限公司北京分公司', LatLng(lat=39.909948, lng=116.376243)),
        (u'绢绢红台湖店', LatLng(lat=39.803114, lng=116.571074)),
        (u'绢绢红涮涮吧', LatLng(lat=39.803334, lng=116.570303)),
        (u'老友记主题咖啡', LatLng(lat=39.920151, lng=116.454427)),
        (u'聚兴隆宾馆', LatLng(lat=39.850230, lng=116.386590)),
        (u'花家怡园', LatLng(lat=39.941224, lng=116.418976)),
        (u'花家怡园·婚宴', LatLng(lat=39.941845, lng=116.418997)),
        (u'花家怡园四合院店', LatLng(lat=39.947299, lng=116.424992)),
        (u'花家怡园外卖店', LatLng(lat=39.941238, lng=116.419088)),
    ]
    t = lib.build_prefix_tree_by_txt(text_file=settings.POI_TXT)
    tested = t.items(prefix='') 
    pp(tested)
    assert expected == tested 
    t = lib.build_prefix_tree_by_txt(text_file=settings.POI_TXT, rank='freq')
    tested = t.items(prefix='')
    expected = [
        (u'Yes煮茶家', 8),
        (u'中化建商务楼', 2),
        (u'乐客主题餐厅', 2),
        (u'乐客主题餐吧', 1),
        (u'公用电话', 1),
        (u'凯菱国际酒店公寓', 3),
        (u'北京中天盈创汽车销售有限公司', 1),
        (u'北京中旺保洁服务', 16),
        (u'北京丰联众合卷烟零售有限责任公司', 1),
        (u'北京市普安兴德商贸中心', 1),
        (u'北京市迪奔汽车维修站', 1),
        (u'北京民文艺术发展公司音像经营部', 1),
        (u'北京祺才人本教育咨询有限责任公司', 1),
        (u'北京腾基伟业网络技术有限公司', 1),
        (u'唐亮工长俱乐部', 3),
        (u'圣米石塘', 5),
        (u'大娘水饺', 1),
        (u'天津奥瑞泰克电子产品有限公司', 1),
        (u'小码头重庆火锅', 1),
        (u'小码头重庆火锅海淀区皂君东里36号', 1),
        (u'悠优乐儿童乐园', 2),
        (u'温馨花店', 2),
        (u'湖南七星电视购物有限公司北京分公司', 1),
        (u'绢绢红台湖店', 1),
        (u'绢绢红涮涮吧', 2),
        (u'绢绢红涮涮吧次渠店', 1),
        (u'老友记主题咖啡', 1),
        (u'老汤拉面', 1),
        (u'聚兴隆宾馆', 1),
        (u'花家怡园', 25),
        (u'花家怡园·婚宴', 1),
        (u'花家怡园八爷府花家店', 1),
        (u'花家怡园四合院店', 1),
        (u'花家怡园外卖店', 1),
        (u'莱莎丽SPA养生连锁机构', 1),
        (u'证通人和教育培训中心', 1),
        (u'诚信防水保温材料', 1),
        (u'豫园家常菜', 2),
        (u'阳光博学教育', 1),
        (u'麦田房产西山别墅豪宅交易服务中心', 1),
    ]
    assert expected == tested 
"""
"""
TO-DO: unlock this block
def test_autocomplete_by_distance(client):
    rv = client.get('/autocomplete?q=北&rank=latlng')
    result = json.loads(rv.data)
    assert 1 == len(result['data'])
    assert 7.92934616043321 == result['data'][0]['priority']
"""
"""
def test_autocomplete_by_freq(client):
    rv = client.get('/autocomplete?q=北&rank=freq')
    result = json.loads(rv.data)
    assert {"name": u"北京中旺保洁服务", "priority": 16} == result['data'][0]
"""

"""
def test_autocomplete_pagination(client):
    rv = client.get('/autocomplete?q=北&rank=freq&page=2')
    result = json.loads(rv.data)
    assert 0  == len(result['data'])
"""
