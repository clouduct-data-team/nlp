# -*- coding: utf-8 -*-
import sys
sys.path.insert(1,'./')
from backend import *
from flask import url_for
from backend import create_app
import pytest
import flask
import urllib2
import simplejson as json
from pprintpp import pprint as pp
from instance import config_test as settings
# http://damyanon.net/flask-series-testing/

class Test_Backend(object):
    def setup_class(self):
        self.app = create_app(settings)
        self.client = self.app.test_client()

    """
    To-Do: Unlock this block
    def test_autocomplete_by_distance(self):
        rv = self.client.get('/autocomplete?q=北&rank=latlng')
        result = json.loads(rv.data)
        assert 1 == len(result['data'])
        assert 7.92934616043321 == result['data'][0]['priority']
    """

    """
    def test_autocomplete_by_freq(self):
        rv = self.client.get('/autocomplete?q=北&rank=freq')
        result = json.loads(rv.data)
        assert {'name': u'北京中旺保洁服务', 'priority': 16} == result['data'][0]

    def test_autocomplete_pagination(self):
        rv = self.client.get('/autocomplete?q=北&rank=freq&page=2')
        result = json.loads(rv.data)
        assert 0  == len(result['data'])

    def test_autocomplete_by_freq_and_city(self):
        rv = self.client.get('/autocomplete?q=北&rank=freq&city=beijing')
        result = json.loads(rv.data)
        assert 8 == len(result['data'])
        rv = self.client.get('/autocomplete?q=雄&rank=freq&city=changde')
        result = json.loads(rv.data)
        assert 1 == len(result['data'])
    """


    """
    def test_autocomplete():
        #res = client.get(url_for('.index'))
        app = create_app()
        client =  app.test_client()
        res = client.get(url_for('.api.backend'))
        assert res.json == {}
    """
