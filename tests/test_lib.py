# -*- coding: utf-8 -*-
import sys
sys.path.insert(1,'../../')
sys.path.insert(1,'../../../')
from flask import url_for
import pytest
import flask
import urllib2
import simplejson as json
#from pprintpp import pprint as pp
import lib
from instance import config_test as settings
# http://damyanon.net/flask-series-testing/
from lib import *
from pprintpp import pprint as pp 
#from utf8_pprint import pp
from utils.data_validator import Txt_Validator

"""
def test_get_cluster_citys():
    result = lib.get_cluster_citys(settings.CLUSTER_PATH, settings.CITY_LIST)
    assert len(result) == len(settings.CITY_LIST)
    result = lib.get_cluster_citys(settings.CLUSTER_PATH, ['Changde', 'Beijing'])
    assert len(result)  == 2 
"""
"""
def test_load_merged_prefix_tree():
    prefix_tree = lib.load_merged_prefix_tree(cluster_path=settings.CLUSTER_PATH, 
                                          data_type=settings.DATA_TYPE, city="Beijing")
    assert 8 == len(prefix_tree.items(prefix=u'北'))

    prefix_tree = lib.load_merged_prefix_tree(cluster_path=settings.CLUSTER_PATH, 
                                          data_type=settings.DATA_TYPE, city="Changde")
    assert 1 == len(prefix_tree.items(prefix=u'雄'))
"""

def test_get_latlng_distance():
    lat1, lon1 = 39.8808, 116.2707
    lat2, lon2 = 39.8708, 116.2807
    assert 1401.6551745107847 == lib.get_latlng_distance(lat1, lon1, lat2, lon2)

def test_validator():
    txt_validator = Txt_Validator()
    response = txt_validator.get_report(txt_file= 'data/sample/Beijing/Beijing.txt')
    assert 16 == response['gid2latlng_error']
    
    txt_validator = Txt_Validator()
    response = txt_validator.get_report(txt_file= 'data/sample/kfc.txt')
    expected  = {'sn2mn_error': 0, 'sn2gid_counts': 47, 
                 'low_latlng_accuracy_error': 1,
                 'gid2sn_error': 0, 'gid2latlng_error': 577} 
    assert expected == response

    txt_validator = Txt_Validator()
    response = txt_validator.get_report(txt_file= 'data/sample/北京自修大学.txt')
    assert 2 == response['sn2mn_error']

def test_merged_data():
    response = lib.get_processed_data(txt_file='data/sample/kfc.txt')
    assert 306 == len(response['mn2sns'][u'肯德基'])
    assert {'lat': 23.0312, 'lng': 113.7771} == response['sn2latlng'][u'肯德基(世博店)']
    assert {} == response['sn2_not_mn']

    response = lib.get_processed_data(txt_file='data/sample/Beijing/Beijing.txt')
    assert 9 == len(response['mn2sns'])
    assert 11 == len(response['sn2latlng'])
    assert {'lat': 39.9745, 'lng': 116.3147} == response['sn2latlng'][u'Yes煮茶家']
    assert 23 == len(response['sn2_not_mn'])

def test_get_merged_structure():
    r1 = lib.get_processed_data(txt_file='data/sample/kfc.txt')
    result = lib.get_merged_structure(mn2sns=r1['mn2sns'], sn2latlng=r1['sn2latlng'], 
                                      sn2_not_mn=r1['sn2_not_mn'])
    assert 306 == result[u'肯德基']['freq']
    assert 306 == len(result[u'肯德基']['shops'])
    assert LatLng(lat=40.0791, lng=116.5896) == result[u'肯德基']['shops'][u'肯德基(机场1店)']

    r2 = lib.get_processed_data(txt_file='data/sample/Beijing/Beijing.txt')
    result = lib.get_merged_structure(mn2sns=r2['mn2sns'], sn2latlng=r2['sn2latlng'], 
                                      sn2_not_mn=r2['sn2_not_mn'])
    assert 32 == len(result)

def test_build_ptree_by_merged_structure():
    r1 = lib.get_processed_data(txt_file='data/sample/kfc.txt')
    result = lib.get_merged_structure(mn2sns=r1['mn2sns'], sn2latlng=r1['sn2latlng'], 
                                      sn2_not_mn=r1['sn2_not_mn'])
    tree = lib.build_ptree_by_merged_structure(result)
    assert 1 == len(tree.items(prefix=u'肯'))
    assert 306 == len(tree.items(prefix=u'肯')[0][1]['shops'])
    
    r2 = lib.get_processed_data(txt_file='data/sample/Beijing/Beijing.txt')
    result = lib.get_merged_structure(mn2sns=r2['mn2sns'], sn2latlng=r2['sn2latlng'], 
                                      sn2_not_mn=r2['sn2_not_mn'])
    tree = lib.build_ptree_by_merged_structure(result)
    assert 2 == len(tree.items(prefix=u'老'))
    assert 8 == len(tree.items(prefix=u'北'))



def test_get_query_cadidate_over10():
    #r1 = lib.get_processed_data(txt_file='../data/sample/head_10k.txt')
    r1 = lib.get_processed_data(txt_file='data/sample/head_10k.txt')
    result = lib.get_merged_structure(mn2sns=r1['mn2sns'], sn2latlng=r1['sn2latlng'], 
                                      sn2_not_mn=r1['sn2_not_mn'])
    tree = lib.build_ptree_by_merged_structure(result)
    # Test for len(items) over 10 
    expected = [
        (u'北京农商银行', 4),
        (u'北京青年旅行社', 2)
    ]
    response = lib.get_query_cadidate(tree, q=u"北", pinyin=False, center=(39.919858, 116.399122))
    assert 10 == len(response[1])
    assert expected == response[1][:2]


def test_get_query_cadidate_between1_10():

    r1 = lib.get_processed_data(txt_file='data/sample/mc_5223.txt')
    result = lib.get_merged_structure(mn2sns=r1['mn2sns'], sn2latlng=r1['sn2latlng'], 
                                      sn2_not_mn=r1['sn2_not_mn'])
    tree = lib.build_ptree_by_merged_structure(result)
    expected = [
            (u'麦当劳', 0),
            (u'麦当劳(双井桥餐厅)', LatLng(lat=39.8951, lng=116.4608)),
            (u'麦当劳(中关村)', LatLng(lat=39.9828, lng=116.3148)),
            (u'麦当劳(丰台北路餐厅)', LatLng(lat=39.8666, lng=116.2938)),
            (u'麦当劳(北京梅市口餐厅)', LatLng(lat=39.8808, lng=116.2707)),
            (u'麦当劳甜品站', 0),
            (u'麦当劳甜品站(东中街)', LatLng(lat=39.9405, lng=116.4358)),
            (u'麦当劳得来速', 0),
            (u'麦当劳得来速(砂石厂店)', LatLng(lat=39.9261, lng=116.2387)),
            (u'麦当劳得来速餐厅', 0)
    ]
    response = lib.get_query_cadidate(tree, q=u"麦当劳", pinyin=False, center=(39.919858, 116.399122))
    assert 10 == len(response[1])
    assert expected == response[1]



def test_get_query_cadidate_one():

    r1 = lib.get_processed_data(txt_file='data/sample/kfc.txt')
    result = lib.get_merged_structure(mn2sns=r1['mn2sns'], sn2latlng=r1['sn2latlng'], 
                                      sn2_not_mn=r1['sn2_not_mn'])
    tree = lib.build_ptree_by_merged_structure(result)

    expected = [
            (u'肯德基(北海店)', LatLng(lat=39.9262, lng=116.3916)),
            (u'肯德基(世都百货店)', LatLng(lat=39.9188, lng=116.4106)),
            (u'肯德基(民航大厦店)', LatLng(lat=39.9137, lng=116.3847)),
            (u'肯德基(建华店)', LatLng(lat=39.9113, lng=116.4115)),
            (u'肯德基(东四店)', LatLng(lat=39.9248, lng=116.4176)),
            (u'肯德基(君太店)', LatLng(lat=39.9153, lng=116.3794)),
            (u'肯德基(东单店)', LatLng(lat=39.912, lng=116.4177)),
            (u'肯德基(西四南大街店)', LatLng(lat=39.9219, lng=116.3737)),
            (u'肯德基(前门东大街店)', LatLng(lat=39.8997, lng=116.4001)),
            (u'肯德基(前门店)', LatLng(lat=39.8997, lng=116.395))
    ]
    response = lib.get_query_cadidate(tree, q=u"肯", pinyin=False, center=(39.919858, 116.399122))
    assert 10 == len(response[1])


def test_get_city_id_mapping():
    response = lib.get_city_id_mapping(settings.MAPPING_PATH)
    expected = {"2": "beijing", "370":"sansha"}
    for k, v in expected.items():
        assert v == response[k]

