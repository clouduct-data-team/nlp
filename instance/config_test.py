
# SERVER_PORT
SERVER_PORT = 80

# Database
DB_HOST = ''
DB_PORT = '3306'
DB_DATABASE = 'dbname'
DB_ID = 'root'
DB_PW = 'testlalatest'

# POI_XML
POI_XML = 'data/sample/Beijing/Beijing.xml'

# POI_TEXT
POI_TXT =  'data/sample/Beijing/Beijing.txt'


# DATA
DATA_PATH = 'data/sample/Beijing/Beijing.txt'

# DATA_TYPE : {XML, TXT}
DATA_TYPE = 'TXT'

DATA_LOAD_LIMIT = 0 

# CLUSTER MODE
LOAD_CLUSTER = True
CLUSTER_PATH = "data/sample"
CITY_LIST = ['Beijing', 'Changde']

# MERGED_TREE
MERGED_TREE = True

# PINYIN
PINYIN = True

# MN_LOAD_FREQ_THRESHOLD
MN_LOAD_FREQ_THRESHOLD = None

# TIMEOUT(sec)
TIMEOUT = None

# CITY_MAPPING_FILE
MAPPING_PATH = "data/city_mapping.txt"

# LOGSTASH
ENABLE_LOGSTASH = True
LOGSTASH_HOST = 'localhost'
LOGSTASH_PORT = 5000

# REDIS_CACHE
ENABLE_CACHE = False
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
