
# SERVER_PORT
SERVER_PORT = 80

# Database
DB_HOST = ''
DB_PORT = '3306'
DB_DATABASE = 'dbname'
DB_ID = 'root'
DB_PW = 'testlalatest'

# POI_XML
POI_XML = 'data/Beijing_lifestyle.xml'
POI_XML_TEST = 'data/Beijing_lifestyle.Sample.xml'

# POI_TEXT
POI_TXT = 'data/Beijing.txt'
POI_TXT_TEST = 'data/Beijing.sample.txt'


# DATA
DATA_PATH = 'data/Beijing.sample.txt'

# DATA_TYPE : {XML, TXT}
DATA_TYPE = 'TXT'

DATA_LOAD_LIMIT = 0

# CLUSTER MODE
LOAD_CLUSTER = False
CLUSTER_PATH = ""
CITY_LIST = []

# MERGED_TREE
MERGED_TREE = False

# PINYIN
PINYIN = True

# MN_LOAD_FREQ_THRESHOLD
MN_LOAD_FREQ_THRESHOLD = 2

# TIMEOUT(sec)
TIMEOUT = None

# CITY_MAPPING_FILE
MAPPING_PATH = "data/city_mapping.txt"

# LOGSTASH
ENABLE_LOGSTASH = True
LOGSTASH_HOST = 'localhost'
LOGSTASH_PORT = 5000

# REDIS_CACHE
ENABLE_CACHE = True
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
