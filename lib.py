# -*- coding: utf-8 -*-
import sys
from flask import make_response,request
import uuid
import xml.etree.ElementTree as ET
from pytrie import StringTrie as trie
from pprintpp import pprint as pp
from random import random
from math import cos, asin, sqrt
import heapq
import operator
import json
import os
import math
import time
from utils.data_validator import Txt_Validator
from utils.data_validator import timeit 
from collections import namedtuple
import collections
from bson import json_util
LatLng = namedtuple('Latlng', ['lat', 'lng'])
Shop = namedtuple('Shop', ['name', 'latlng'])


def build_prefix_tree(file_path=None, file_type=None, load_limit=0, rank=None):
    if file_type is "TXT":
        return build_prefix_tree_by_txt(text_file=file_path, load_limit=load_limit, rank=rank)
    elif file_type is "XML":
        return build_prefix_tree_by_xml(xml_file=file_path, load_limit=load_limit, rank=rank)

def get_cluster_citys(cluster_path, load_city_list):
    city_list = load_city_list
    if city_list == []:
        city_list = os.listdir(cluster_path)
    return city_list

def get_city_id_mapping(mapping_file):
    """
    {
      '1': 'shanghai',
      '2': ' beijing',
      ..
    }
    """
    mapping = {}
    with open(mapping_file) as f:
        for line in f:
            # 北京    2       Beijing
            city, cityid, cityname = line.split("\t")
            mapping[cityid] = cityname.lower().strip()
    return mapping 

def get_cityname_id_mapping(mapping_file):
    """
    {
      北京: '2',
      ..
    }
    """
    mapping = {}
    with open(mapping_file) as f:
        for line in f:
            # 北京    2       Beijing
            city, cityid, cityname = line.split("\t")
            mapping[city.strip().decode('utf-8')] = int(cityid)
    return mapping 
    
"""
def load_merged_prefix_tree(cluster_path, data_type, city):
    file_path = "%s/%s/%s.txt" % (cluster_path, city, city)

    if data_type is "TXT":
        return build_prefix_tree_by_txt(text_file=file_path, rank="merged")
    elif data_type is "XML":
        return build_prefix_tree_by_xml(xml_file=file_path, rank="merged")
"""


def get_processed_data(txt_file=None, load_limit=0 , load_validator=None):
    mn2sns = {}
    sn2_not_mn = {}
    sn2latlng = {}

    if load_validator:
        validator = Txt_Validator()

    def get_cleaned_dict(line, load_validator=None):
        if load_validator:
            validator.validate_line(line)

        line = line.strip().replace(":\"0\"\"","").\
                            replace(":\"-1\"\"","")

        try:
            obj = json.loads(line)
        except Exception as e:
            return None

        if 'lat' in obj:
            gid = obj['groupid']
            if  len(obj['lat']) <  7 or len(obj['lng']) <  7:
                pass
            try:
                lat = round(float(obj['lat']), 4)
                lng = round(float(obj['lng']), 4)
                obj['lat'], obj['lng'] = lat, lng 
            except Exception as e:
                obj.pop('lat', None)
                obj.pop('lng', None)

        return obj

    with open(txt_file) as f:
         for idx, line in enumerate(f, 1):
            if load_limit is not 0 and idx > load_limit:
                break

            obj = get_cleaned_dict(line, load_validator)

            if obj is None or 'showname' not in obj:
                continue
            showname = obj['showname']
            if showname not in sn2_not_mn:
                sn2_not_mn[showname] = True 
            if 'showmainname' in obj:
                showmainname = obj['showmainname'] 
                if showname in sn2_not_mn:
                    sn2_not_mn.pop(showname, None)
                if showmainname not in mn2sns:
                    mn2sns[showmainname] = []
                if showname not in mn2sns[showmainname]:
                    mn2sns[showmainname].append(showname)
            if 'lat' in obj and 'lng' in obj:
                if showname not in sn2latlng:
                    sn2latlng[showname] = {}
                    sn2latlng[showname]['lat'] = obj['lat']
                    sn2latlng[showname]['lng'] = obj['lng']

    result = {}
    result['mn2sns'] = mn2sns
    result['sn2latlng'] = sn2latlng
    result['sn2_not_mn'] = sn2_not_mn
    return result

def get_merged_structure(mn2sns=None, sn2latlng=None, sn2_not_mn=None):
    """
    {
        mainname: {
            freq: 32  # length of shops >= 1
            shops: {
                name: latlng,
                ..
            }
        }
    }
    """
    def get_latlng_from_sn2latlng(sn):
        lat, lng = None, None
        if sn in sn2latlng:
            lat, lng = sn2latlng[sn]['lat'], sn2latlng[sn]['lng']
        return lat, lng
    
    result = {}
    for sn in sn2_not_mn:
        result[sn] = {}
        result[sn]['freq'] = 1
        result[sn]['shops'] = {}
        lat, lng = get_latlng_from_sn2latlng(sn)
        latlng = LatLng(lat=lat, lng=lng)
        #shop = Shop(name=sn, latlng=latlng)
        result[sn]['shops'][sn] = latlng 

    for mn, shops in mn2sns.iteritems():
        result[mn] = {}
        result[mn]['freq'] = len(shops)
        result[mn]['shops'] = {}
        for sn in shops:
            lat, lng = get_latlng_from_sn2latlng(sn)
            latlng = LatLng(lat=lat, lng=lng)
            result[mn]['shops'][sn] = latlng 
    return result

def build_ptree_by_merged_structure(merged_result,  mn_load_freq_threshold=None,  pinyin=False ):
    """
    merged_result = 
    {
        mainname: {
            freq: 32  # length of shops >= 1
            shops: {
                name: latlng,
                ..
            }
        }
    }
    tree = 
    """
    if pinyin:
        from pypinyin import pinyin, lazy_pinyin, FIRST_LETTER
        import pypinyin
        import itertools 

    t = trie()
    for mn, meta in merged_result.iteritems():
        if mn_load_freq_threshold is not None and meta['freq'] < mn_load_freq_threshold:
            continue
        if pinyin:
            # and len(meta) >= 2:
            pinyin_long = "".join(lazy_pinyin(mn))
            pinyin_abbr_list  = pinyin(mn, heteronym=False, style=pypinyin.FIRST_LETTER)
            # http://stackoverflow.com/questions/716477/join-list-of-lists-in-python
            # To join list of list [[u'z'], [u'x']]
            pinyin_abbr = "".join(list(itertools.chain(*pinyin_abbr_list)))
            if pinyin_long not in t:
                t[pinyin_long] = {}
            t[pinyin_long][mn] = meta
            """
            t[kfc] = {
                "肯德基": { 'freq': xxx, 'shops': { ... } },
                "看佛區": { 'freq': xxx, 'shops': { ... } },
                ...
            }

            """

            if pinyin_abbr not in t:
                t[pinyin_abbr] = {}
            t[pinyin_abbr][mn] = meta

        else:
            t[mn] = meta 
    return t 


def get_all_freq(pinyin, items):
    all_freq = 0
    visited_mn = {}

    if pinyin:
        for item in items:
            for name, meta in item[1].iteritems():
                if name in visited_mn:
                    continue
                visited_mn[name] = True
                all_freq += meta['freq']
    else:
        for item in items:
            all_freq += item[1]['freq']

    return all_freq 

def get_top_shops_for_mainnames(tree, items, pinyin, q, k, center):
    all_freq = 0
    candidate = []
    if pinyin:
        item_generator = name_meta_pinyin_generator
    else:
        item_generator = name_meta_hanzi_generator 

    all_freq = get_all_freq(pinyin, items)
    items  = tree.iteritems(prefix=q)

    mn_k, mn_shops = {}, {}
    for item in items:
        for mn, meta in item_generator(item):
            top_shops_of_mn = int(round(float(len(meta['shops'])) / all_freq * \
                    k, 1))

            shops_dict = meta['shops']
            mn_k[mn] = top_shops_of_mn
            mn_shops[mn] = get_nearst_shops(shops_dict, center, top_shops_of_mn)
            insert_mn = True

            for idx, shop in enumerate(mn_shops[mn]):
                if insert_mn and mn == shop[0]:
                    del mn_shops[mn][idx]
                    mn_shops[mn].insert(0, (mn, 0))
                    insert_mn = False
                    break
            if insert_mn:
                mn_shops[mn].insert(0, (mn, 0))

    sorted_mn_k = sorted(mn_k.items(), key=operator.itemgetter(1), \
                reverse=True)
    counts = 0
    for mn, top_shops_of_mn in sorted_mn_k:
        candidate = candidate + mn_shops[mn]
        counts += top_shops_of_mn
        if counts >= k:
            break
    return candidate

def get_nearst_shops(shops_dict, center, k=None):
    merge_list = []
    heap = []
    non_latlng = []
    if k is None:
        k = len(shops_dict)
    for name, latlng in shops_dict.iteritems():
        distance = compute_distance(center, latlng)
        if distance is not None:
            # ignore None would cause insuffcient return counts 
            heapq.heappush(heap, (distance*(-1), (name, latlng)))
        else:
            non_latlng.append((name, latlng))
    sorted_latlng = [tup[1] for tup in heapq.nlargest(k, heap)]
    merge_list  = sorted_latlng + non_latlng
    return merge_list[:k]


def get_mainname_counts_for_query(items, q, pinyin, k):
    if pinyin:
        item_generator = name_meta_pinyin_generator
    else:
        item_generator = name_meta_hanzi_generator 

    counts = 0
    break_lock = False
    visited_mn = {}

    for item in items:
        for name, meta in item_generator(item):
            # Dedupe anme name appears in differnet pinyn_long, pyinin_addr
            if name not in visited_mn:
                #print "\t", name
                visited_mn[name] = True
                counts += 1
                if counts > k:
                    break_lock = True
                    break
        if break_lock:
            break
    return counts


def get_topk_shops_for_certain_mainname(items, pinyin, center, k):
    # case 3
    candidate = []
    
    if pinyin:
        item_generator = name_meta_pinyin_generator
    else:
        item_generator = name_meta_hanzi_generator 

    item = items.next()
    for mn, meta in item_generator(item):
        #mn, meta = item[0], item[1]
        shops_dict = meta['shops']
        candidate = get_nearst_shops(shops_dict, center)[:k]
        if mn not in candidate:
            candidate.insert(0, (mn, 0))
    return candidate
    

def name_meta_hanzi_generator(item):
    """
    items  = {
                "肯德基": {'freq': 12, 'shops': {'xxx': latlng, ..},
                "可得基": .. 
             }
    """
    name, meta = item[0], item[1]
    yield name, meta

def name_meta_pinyin_generator(item):
    """
    items = "kfc": {   
                "肯德基": {'freq': 12, 'shops': {'xxx': latlng, ..}
                "可得基": .. 
            },
            "kfcdlx": {
                "肯德基得來速": .....
            }
    """
    for main, meta  in item[1].iteritems():
        yield main, meta 

def name_freq_hanzi_generator(item):
    for name, meta in name_meta_hanzi_generator(item):
        freq = meta['freq']
        yield name, freq

def name_freq_pinyin_generator(item): 
    for name, meta in name_meta_pinyin_generator(item):
        freq = meta['freq']
        yield name, freq

def get_topk_mainname(items, k, pinyin, start_time, timeout):
    heap = []
    candidate = []
    break_lock = False
    visited_mn = {}

    if pinyin:
        item_generator = name_freq_pinyin_generator
    else:
        item_generator = name_freq_hanzi_generator

    for item in items:
        for name, freq in item_generator(item):
            if pinyin:
                if name in visited_mn:
                    continue
                visited_mn[name] = True

            if timeout != None and (time.time() - start_time) > timeout:
                break_lock = True
                break

            if len(heap) < k  or freq >  heap[0][0]:
                if len(heap) == k:
                    heapq.heappop( heap )
                heapq.heappush( heap, (freq, (name, freq)))

        if  break_lock:
            break

    candidate = [tup[1] for tup in heapq.nlargest(k, heap)]
    return candidate
    
def get_query_cadidate(tree, q, pinyin, center, page=None, timeout=None):
    if page is None:
        page = 1
    if q == u"":
        return []
    start_time = time.time()
    #items = tree.items(prefix=q)
    items = tree.iteritems(prefix=q)
    k = 10
    item_per_page = k
    counts = get_mainname_counts_for_query(items, q, pinyin,  k)
    items  = tree.iteritems(prefix=q)

    case_type = 0
    candidate = []
    if counts  >= k:
        # return top 10 brains with most shops
        case_type = 1
        candidate = get_topk_mainname(items, k, pinyin, start_time, timeout)
    elif counts > 1:
        case_type = 2
        candidate = get_top_shops_for_mainnames(tree, items, pinyin, q, k, center)
    elif counts == 1:
        case_type = 3
        candidate = get_topk_shops_for_certain_mainname(items, pinyin, center, k)
    elif counts == 0:
        print "case 4"
        case_type = 4
        # TO-DO: Add backtrace : search elasticsearch and get answer
        pass
    return case_type, candidate[(page-1)*item_per_page : page*item_per_page]

@timeit
def build_prefix_tree_by_txt(text_file=None, load_limit=0, rank=None):

    if text_file is None:
        text_file = '../data/Beijing.sample.txt'

    t = trie()
    i = 0
    error_count = 0 
    with open(text_file) as f:
         for line in f:
            i += 1
            #if i ==  30:
            #    break

            line = line.strip().replace(":\"0\"\"","").\
                                replace(":\"-1\"\"","")
            try:
                obj = json.loads(line)
                shop_name = obj["name"].split(u"(")[0]
            except Exception as e:
                #for idx, j in enumerate(line):
                #    print idx,j
                print "line", i
                print str(e)
                error_count += 1
                print "error_count", error_count    

            if rank is "freq":
                if shop_name not in t:
                    t[shop_name] = 0
                t[shop_name] += 1

            elif rank in [None, 'latlng']:
                 if 'lat' in obj and 'lng' in obj:
                    lat, lng  = obj['lat'], obj['lng']
                    #print "%s,%s" % (lat, lng)
                    latlng = LatLng(lat=float(obj['lat']), lng=float(obj['lng']))
                    t[shop_name] = latlng
                    #t[shop_name] = "%s,%s" % (lat, lng)

            elif rank is "merged":
                if "showmainname" in obj:
                    shop_name = obj["showmainname"]
                else:
                    shop_name = obj["name"]

                if shop_name not in t:
                    t[shop_name] = {}
                    t[shop_name]['freq'] = int(obj["tally"])
                    t[shop_name]['data'] = []
                #t[shop_name]['freq'] += 1
                if 'lat' in obj and 'lng' in obj:
                    try:
                        latlng = LatLng(lat=float(obj['lat']), lng=float(obj['lng']))
                        tmp = {'name': obj['name'], 'latlng': latlng}
                        t[shop_name]['data'].append(tmp)
                    except:
                        print type(obj['lat']), obj['lat']
                        print type(obj['lng']), obj['lng']
                        continue
                    #t[shop_name]['latlng'] = "%s,%s" % (obj['lat'], obj['lng'])

            if i%10000==0:
                print i

            if load_limit != 0 and load_limit == i:
                break
    #pp(t.items(prefix=''))
    #print error_count, i
    #from guppy import hpy
    #h=hpy()
    #pp(h.heap())
    return t

@timeit
def build_prefix_tree_by_xml(xml_file=None, load_limit=0, rank=None):

    if xml_file is None:
        xml_file = 'Beijing_lifestyle.Sample.txt'

    tree = ET.parse(xml_file)
    root = tree.getroot()
    t = trie()
    i = 0
    for doc in root.findall('doc'):
        i += 1
        if rank is "freq":
            shop_name = doc.find("field/[@name='name']").text.split(u"(")[0]
            if shop_name not in t:
                t[shop_name] = 1
            else:
                t[shop_name] += 1

        elif rank in [None, "latlng"]:
            name = doc.find("field/[@name='name']")
            loc = doc.find("field/[@name='loc']")

            if loc is not None:
                # print "%s(%s) - [%s]" % (name.text, id.text, loc.text)
                lat, lng = loc.text.split(',')
                latlng = LatLng(lat=float(lat), lng=float(lng))
                t[name.text] = latlng
                #t[name.text] = loc.text
            else:
                pass
    
        elif rank is "merged":
            shop_name = doc.find("field/[@name='name']").text.split(u"(")[0]
            if shop_name not in t:
                t[shop_name] = {}
            if 'freq' not in t[shop_name]:
                t[shop_name]['freq'] = 0
            t[shop_name]['freq'] += 1
            if 'lat' in obj and 'lng' in obj:
                loc = doc.find("field/[@name='loc']")
                if loc is not None:
                    lat, lng = loc.text.split(',')
                    latlng = LatLng(lat=float(lat), lng=float(lng))
                    t[name.text] = latlng
                    #t[shop_name]['latlng'] =  loc.text

        if load_limit != 0 and load_limit == i:
            break

    return t

def get_latlng_distance(lat1, lon1, lat2, lon2):
    '''
    Haversine Formula in Python (Bearing and Distance between two latlng points)
    Reference: http://tinyurl.com/l3tdwma
    '''
    p = 0.017453292519943295
    a = 0.5 - cos((lat2 - lat1) * p)/2 + \
            cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a)) * 1000

def compute_distance(center, poi):
    lat, lng = poi.lat, poi.lng
    if lat is None or lng is None:
        return None
    #lat, lng = latlng_poi.split(",")
    return get_latlng_distance(center[0], center[1], lat, lng)
    #return get_latlng_distance(center[0], center[1], float(lat), float(lng))

@timeit
def sort_trie_items_by_distance_binary_sort(trie_items, center=None):
    if center is None:
        raise Exception('spam', 'eggs')
    poi_d = {}

    for item in trie_items:
        poi_d[item[0]] = compute_distance(center, item[1])

    import operator
    sorted_poi = sorted(poi_d.items(), key=operator.itemgetter(1))
    return sorted_poi

@timeit
def sort_trie_items_by_distance_heap_sort(trie_items, center, merged, item_range):
    k = item_range
    if center is None:
        raise Exception('spam', 'eggs')
    heap = []
    # customize heap sort
    # Ref: http://stevehanov.ca/blog/index.php?id=122
    # Ref: https://docs.python.org/2/library/heapq.html#basic-examples
    print "0"*20
    pp(trie_items)
    for item in trie_items:
        if merged == True:
            if 'latlng' not in item[1]:
                continue
            name = item[0]
            latlng = item[1]['latlng']
        else:
            name = item[0]
            latlng = item[1]

        distance = compute_distance(center, latlng) 
        if len(heap) < k or 1/distance >  heap[0][0]:
            if len(heap) == k:
                heapq.heappop( heap )
            heapq.heappush( heap, (1/distance, (name, distance)))
    print "1"*20
    pp(heap)
    print "2"*20
    candidate = [tup[1] for tup in heapq.nlargest(k, heap)]
    pp(candidate)
    print "3"*20
    return candidate

@timeit
def get_query_result2(q=None, center=None, trie=None, merged=False, rank=None, page=None, pinyin=False, timeout=None):
    case_type, result = get_query_cadidate(tree=trie, q=q, pinyin=pinyin, center=center, page=page, timeout=timeout)
    return {'case_type': case_type, 'total': len(result), 'data': result}

@timeit
def get_query_result(q=None, center=None, trie=None, merged=False, rank=None, page=None):
    if trie is None or q is u'':
        return {'total': 0, 'data': []}

    PER_PAGE = 10
    page = (1 if page is None else int(page))
    item_range = page * PER_PAGE
    center = ((39.919858, 116.399122) if center is None else center)

    items = trie.items(prefix=q)
    #pp(items)
    if rank in [None, "latlng"]:
        result = sort_trie_items_by_distance_heap_sort(items, center, merged, item_range)
    elif rank is "freq":
        #if len(items) > 1000:
        result = heap_sort(items, item_range, merged)
            #result = heapq.nsmallest(items, item_range, key=operator.itemgetter(1))
        #else:
        #    items.sort(key=lambda tup: tup[1], reverse=True)
        #    result = items
    result = result[PER_PAGE*(page-1):PER_PAGE*page]
    return {'total': len(items), 'data': result}


def heap_sort( bigArray, k , merged):
    # Using heap sort instead of binary search ( default sort())
    # Ref: https://docs.python.org/2/library/heapq.html#basic-examples
    # Ref: http://stevehanov.ca/blog/index.php?id=122
    heap = []
    # Note: below is for illustration. It can be replaced by
    # heapq.nlargest( bigArray, k )
    # TO-DO: See whether exists a heap sort w/ pagination algo
    for item in bigArray:
        if merged == True:
            name = item[0]
            freq = item[1]['freq']
        else:
            name = item[0]
            freq = item[1]

        if len(heap) < k or freq  > heap[0][0]:
            if len(heap) == k:
                heapq.heappop( heap )
            heapq.heappush( heap, (freq, (name, freq)))
    sorted_queue  = [tup[1] for tup in heapq.nlargest(k, heap)]
    return sorted_queue 

@timeit 
def generate_json(f):
    _ = json.dumps(f, skipkeys=False, ensure_ascii=True, check_circular=True,
                   allow_nan=True, cls=None, indent=True, separators=None,
                   encoding="utf-8", sort_keys=False, default=json_util.default)

    return _

def generate_response_from_json(json_str):
    resp = make_response(json_str)
    if request.headers.get('Accept', '').find('application/json') > -1:
        resp.mimetype = 'application/json'
    else:
        resp.mimetype = 'text/plain'
    return resp
